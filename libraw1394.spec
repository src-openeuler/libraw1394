Name:           libraw1394
Version:        2.1.2
Release:        10
Summary:        Provides direct access to the IEEE 1394 bus
License:        LGPLv2+
URL:            http://www.dennedy.org/libraw1394/
Source:         http://www.kernel.org/pub/linux/libs/ieee1394/%{name}-%{version}.tar.xz
Recommends:	%{name}-help = %{version}-%{release}
BuildRequires:  gcc kernel-headers chrpath

%description
Libraw1394 is the only supported interface to the kernel side raw1394 of
the Linux IEEE-1394 subsystem, which provides direct access to the connected
1394 buses to user space.  Through libraw1394/raw1394, applications can directly
send to and receive from other nodes without requiring a kernel driver for the
protocol in question.

%package devel
Summary:        Development files for libraw1394
Requires:       %{name} = %{version}-%{release} pkgconfig

%description devel
This package contains libraries and header files for libraw1394.

%package help
Summary:        Help documents for libraw1394

%description help
This Package contains man pages and other files for libraw1394.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --disable-static
%make_build

%install
%make_install
%delete_la

chrpath -d %{buildroot}%{_bindir}/*

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%license COPYING.LIB
%doc README NEWS
%{_libdir}/libraw1394.so.*
%{_bindir}/{dumpiso,sendiso,testlibraw}

%files devel
%doc doc/libraw1394.sgml
%{_includedir}/libraw1394/
%{_libdir}/libraw1394.so
%{_libdir}/pkgconfig/libraw1394.pc

%files help
%{_mandir}/man1/*
%{_mandir}/man5/isodump.5.gz

%changelog
* Tue Oct 10 2023 chenchen <chen_aka_jan@163.com> - 2.1.2-10
- remove rpath for the commond

* Thu Nov 12 2020 xinghe <xinghe1@huawei.com> - 2.1.2-9
- add help for Recommends

* Fri Oct 25 2019 yanzhihua <yanzhihua4@huawei.com> - 2.1.2-8
- Package init

